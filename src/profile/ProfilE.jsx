import React, { Component } from 'react'
import image from '../image.jpeg'


export default class ProfilE extends Component {
    state={
        fullname:"zahara",
        bio:"je suis developpeur",
        profession:"developpeur",
        show: true
    }
    change =()=>this.setState({
        show: !this.state.show
    })
    render() {
        if (!this.state.show) {
            return (
                <div style={{width:"20%", height:"300px", border:"2px solid black" ,backgroundColor:"grey"}}>
                <img style={{width:"60%", height:"130px", borderRadius:"20px"}} src={image} alt="" />
                <h1>{this.state.fullname}</h1>
                <p>{this.state.bio}</p>
                <button style={{width:"30%",height:"30px", backgroundColor:"red", border:"none", borderRadius:"20px", }} onClick={this.change}>activer</button>
                </div>
    )  
        } else{ return <button style={{width:"30%",height:"30px",
        backgroundColor:"red", border:"none", borderRadius:"20px", }}
        onClick={this.change}>masquer</button>
    }
     
}
}
