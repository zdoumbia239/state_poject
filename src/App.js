import logo from './logo.svg';
import './App.css';
import  image from "./image.jpeg"
import ProfilE from './profile/ProfilE';

function App() {
  return (
    <div className="App" style={{textAlign:"center",padding:"100px", display :"flex",  justifyContent:"center"}}>
    <ProfilE>{image}</ProfilE>
    </div>
  );
}

export default App;
